<?php

namespace App\Http\Controllers;

use Cart;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Stripe\Charge;
use Stripe\Stripe;

class CheckoutController extends Controller
{
    public function index(){

        if (Cart::content()->count() == 0){
            Session::flash('info', 'your cart is empty, do some shopping');
            return redirect()->back();
        }
        return view('checkout');
    }

    public function pay(){
        Stripe::setApiKey('sk_test_51IaXIPFkV4cxTkRfxiJP9XQYqWMmYQGhwCHRbIoDgs96Vt4nCSqUnfwHTmgpp2v6gqSN4BNHfGR64W7axvBO26Vh009qwrNUyg');
        $charge = Charge::create([
            'amount' => Cart::total() *100,
            'currency' => 'usd',
            'description' => 'laravel testing ecom site',
            'source' => request()->stripeToken
        ]);
        Session::flash('success','purchase successful,wait or out email');
        Cart::destroy();
        Mail::to(request()->stripeEmail)->send(new \App\Mail\PurchaseSuccessful);
        return redirect('/');
    }
}
